Wolfang's Web-Crawler
=====================

A multi-threaded web-crawler for extracting links

Example
--------

Extract all links from https://www.apple.com/tr/ up to 2 levels of depth, store in the user folder, use 50 workers per processor core, follow robots.txt rules, and apply a regex that will only allow urls that starts with /tr/i*

wolfcrawler -d 2 -o ~ -w 50 -R --regex "(\w+):\/\/([\w\.]+)\/(tr\/i[\w\/\?\=\:\-\&\%]+)" https://www.apple.com/tr/
