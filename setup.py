# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='WolfCrawler',
    version='1.2.0',
    description='A simple web-crawler',
    long_description=long_description,
    author='Wolfang Torres',
    author_email='wolfang.torres@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],
    packages=['WolfCrawler'],
    install_requires=['click', 'bs4'],
    entry_points={
        'console_scripts': [
            'wolfcrawler=WolfCrawler:process_sites',
        ],
    },
)

