#!/usr/bin/env python3
"""
WolfCrawler

utility for extracting webpages links
"""

import logging
import re
import sys
from time import time, sleep, strftime
from os import cpu_count, path, getcwd
from collections import deque
from multiprocessing import Event
from multiprocessing.pool import ThreadPool
from threading import Lock
from queue import Queue, Empty
from csv import writer
from socket import timeout
from urllib.robotparser import RobotFileParser
from urllib.request import build_opener
from urllib.parse import urlparse, urljoin, urlunparse
from urllib.error import URLError
from http.client import BadStatusLine
import pip
try:
    from bs4 import BeautifulSoup
except ImportError:
    if pip.main(['install', '--user', 'bs4']):
        pip.main(['install', 'bs4'])
    from bs4 import BeautifulSoup
try:
    from tld import get_tld
except ImportError:
    if pip.main(['install', '--user', 'tld']):
        pip.main(['install', 'tld'])
    from tld import get_tld


logging.getLogger().setLevel(logging.INFO)

# Constans
RATE_OF_LOGGER = 50 # number of nodes before logging status
TIMEOUT = 60 # time (seconds) to wait for page to load before restarting node
# Regex
GENERAL_URL = re.compile(r"(?P<scheme>\w+:\/\/)?(?P<netloc>[\w\.]+)(?P<path>\/?[\w\/\?\=\:\-\&\%]+)?")
# Defaults
DEFAULT_DEPTH = 2 # maximun depth for extration
DEFAULT_OUTPUT = None # None selects the current working directory
DEFUALT_WOKERS = 100 # number of thread per procesor core to be use
DEFAULT_ROBOTS = True
DEFAULT_DUPLICATES = False
DEFAULT_EXTERNAL = False
DEFUALT_REGEX = None


class BasicFilter:
    """
    Basic filter that:
    - doesn't allows duplicates
    - doest allows external sites
    - follows robots.txt
    """

    def __init__(self, site, database, robots, duplicates, external, regex):
        self.site = site
        self.database = database
        self.robots = robots
        self.duplicates = duplicates
        self.external = external
        self.regex = regex
        if robots:
            self.rp = RobotFileParser()
            robots_url = urljoin(site.geturl(), '/robots.txt')
            self.rp.set_url(robots_url)
            logging.info('Reading contens of {}, please wait'.format(robots_url))
            try:
                self.rp.read()
                self.crawl_delay = self.rp.crawl_delay('wolfcrawler')
            except Exception as err:
                logging.error("Cound't read robots.txt file due to: %s", err)
                self.robots = False
        if regex:
            self.pattern = re.compile(regex)

    def __call__(self, node):
        if not self.external:
            if node['url'].netloc != self.site.netloc:
                return False
            if not node['url'].path.startswith(self.site.path):
                return False
        if not self.duplicates:
            if self.database.check_node(node):
                return False
        if self.robots:
            if not self.rp.can_fetch('wolfcrawler', node['url'].geturl()):
                return False
            sleep(self.crawl_delay)
        if self.regex:
            if not self.pattern.match(node['url'].geturl()):
                return False
        return True


class FileDatabase():
    """A file-based database"""

    def __init__(self, site, folder=None):
        self.site = site
        self.folder = folder if not folder is None else getcwd()
        self.lock = Lock()
        self.manifest = []

    @property
    def filename(self):
        return path.join(self.folder, '{}.csv'.format(self.site.netloc))

    def add_node(self, node):
        with self.lock:
            self.manifest.append(node['url'])

    def check_node(self, node):
        with self.lock:
            return node['url'] in self.manifest

    def store(self):
        logging.info('Saving Urls to %s', self.filename)
        with open(self.filename, 'w') as csvfile:
            csv_writer = writer(csvfile, delimiter=',', quotechar='"')
            for url in self.manifest:
                csv_writer.writerow([url.geturl()])


class Worker:
    """Thread based node traverser"""

    def __init__(self, max_depth, opener, database, _filter, execution_flag, name):
        self.max_depth = max_depth
        self.opener = opener
        self.database = database
        self._filter = _filter
        self.execution_flag = execution_flag
        self.name = name

    def __call__(self, node, node_number):
        self.execution_flag.wait()
        try:
            starting_time = time()
            logging.debug('{} Starting node {}: (level {}) {}'.format(
                self, node_number, node['depth'], node['url'].geturl()))
            childrens = self._process_node(node)
            filtered = list(filter(self._filter, childrens))
            return filtered
        except KeyboardInterrupt:
            logging.warning('{} Catched KeyboardInterrupt'.format(self))
        finally:
            total_time = time() - starting_time
            logging.debug('{} finished on {} sec'.format(self, total_time))

    def _process_node(self, node):
        childrens = []
        if node['depth'] < self.max_depth:
            while True:
                try:
                    with self.opener.open(node['url'].geturl(), timeout=TIMEOUT) as page:
                        soup = BeautifulSoup(page, 'html.parser')
                        for link in soup.find_all('a'):
                            if not link.get('href'):
                                continue # ignore empty
                            if link.get('href')[0] == '#':
                                continue # ignore fragments
                            url = urlparse(urljoin(node['url'].geturl(), link.get('href')))
                            url = urlparse(urlunparse(url[:-1]+('',)))
                            if not GENERAL_URL.fullmatch(url.geturl()):
                                continue # not valid URL
                            childrens.append({
                                'url':url,
                                'depth':node['depth']+1,
                                })
                except BadStatusLine:
                    pass
                else:
                    self.database.add_node(node)
                    break
        return childrens

    def __str__(self):
        return "P{}".format(self.name)


class WebCrawler:
    """Manages the subprocess necesary to build a tree"""

    def __init__(self, workers_per_core=DEFUALT_WOKERS):
        logging.info('Creating a WebCrawler')
        self.execution_flag = Event()
        self.total_worker_count = 0
        self.total_time = 0
        self.total_sites_processed = 0
        self.max_workers = int(cpu_count() * workers_per_core)
        self.opener = build_opener()
        self.opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        logging.info(('There are {} cores on sistem, '
                      'using {} workers max'
                      ).format(cpu_count(), self.max_workers))

    def process(self, site, max_depth, database, _filter):
        try:
            starting_time = time()
            logging.info('At {} Processing site {}'.format(
                strftime("%a, %d %b %Y %H:%M:%S"), site.geturl()))
            workers = self._prepare_workers(max_depth, database, _filter)
            workers_count = self._process(site, workers, starting_time)
        except:
            raise
        else:
            self.total_worker_count += workers_count
            self.total_sites_processed += 1
            total_time = time() - starting_time
            self.total_time += total_time
            logging.info('Finished, processed {} nodes on {} seconds'.format(
                workers_count, total_time))
        finally:
            database.store()

    def _prepare_workers(self, max_depth, database, _filter):
        workers = deque()
        for i in range(self.max_workers):
            workers.append(Worker(max_depth, self.opener, database, _filter, self.execution_flag, i))
        return workers

    def _log_status(self, starting_time, nodes_procesed, workers_count):
        t = time() - starting_time
        p = t / nodes_procesed if nodes_procesed > 0 else 0
        logging.info((
            "{} nodes procesed in {} sec "
            "for an aprox. of {} sec per node. "
            "{} nodes remain for processing "
            "right now ").format(
                nodes_procesed, t, p,
                workers_count-nodes_procesed))

    def _process(self, site, workers, starting_time):
        nodes = Queue()
        nodes.put({
            'url':site,
            'depth':0,
            })
        results = []
        workers_count = 0
        nodes_procesed = 0
        with ThreadPool(processes=self.max_workers) as pool:
            self.execution_flag.set()
            while True:
                for node, result in results:
                    if result.ready():
                        results.remove((node, result))
                        try:
                            for childs in result.get():
                                nodes.put(childs)
                        except (timeout, URLError) as err:
                            print(err, type(err))
                            logging.error('"{}" ocurred on node <{}>. returning for re-procesing'.format(err, node['url'].geturl()))
                            nodes.put(node)
                        except UnicodeEncodeError as err:
                            logging.error("<%s> node can't be processed due to '%s", node['url'].geturl(), err)
                        except:
                            raise
                        else:
                            nodes_procesed += 1
                        if nodes_procesed % RATE_OF_LOGGER == 0:
                            self._log_status(starting_time, nodes_procesed, workers_count)
                while True:
                    try:
                        node = nodes.get_nowait()
                        workers.rotate()
                        workers_count += 1
                        results.append((node, pool.apply_async(workers[0], (node, workers_count))))
                    except Empty:
                        break
                if len(results) == 0 and nodes.empty():
                    break
                sleep(0.1/self.max_workers)
        return workers_count


def process_url(url):
    match = GENERAL_URL.search(url)
    if not match:
        raise URLError('%s is not a valid url, please enter a valid one'%url)
    data = match.groupdict()
    if data['scheme'] is None:
        data['scheme'] = 'http://'
    res = get_tld(data['netloc'], as_object=True, fix_protocol=True)
    if not res.subdomain:
        data['netloc'] = 'www.'+data['netloc']
    if data['path'] is None:
        data['path'] = '/'
    completed_url = data['scheme']+data['netloc']+data['path']
    site = urlparse(completed_url)
    logging.info('URL procesed to %s', site.geturl())
    return site

def process_sites(webpages,
        depth=DEFAULT_DEPTH,
        output=DEFAULT_OUTPUT,
        workers=DEFUALT_WOKERS,
        robots=DEFAULT_ROBOTS,
        duplicates=DEFAULT_DUPLICATES,
        external=DEFAULT_EXTERNAL,
        regex=DEFUALT_REGEX):
    logging.info('''Starting Extraction for {webpages},
    extracting {depth} levels,
    outputing to {output},
    using {workers} workers per core,
    folowing robots.txt: {robots},
    allowing duplicates: {duplicates},
    allowing external sites: {external},
    custom regex: {regex},
    '''.format(**locals()))
    web_crawler = WebCrawler(workers)
    try:
        for website in webpages:
            try:
                site = process_url(website)
                _database = FileDatabase(site, output)
                _filter = BasicFilter(site, _database, robots, duplicates, external, regex)
                web_crawler.process(site, depth, _database, _filter)
            except KeyboardInterrupt:
                logging.warning('Safely stoping file procesing')
                break
    finally:
        logging.info((
            'Processed {} websites, '
            'for a total of {} nodes '
            'in {} minutes').format(
                web_crawler.total_sites_processed,
                web_crawler.total_worker_count,
                web_crawler.total_time/60))


if __name__ == '__main__':
    webpages = sys.argv[1:]
    #webpages = ['sahibinden.com']
    if webpages:
        process_sites(webpages)
    else:
        print("Usage: python3 wolfcrawler.py url")
    
