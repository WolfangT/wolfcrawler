"""
web-crawler

utility for extracting webpages
"""

import logging
from urllib.parse import urlparse

import click

from . import filters
from . import database
from .extractor import WebCrawler
from .constants import MAX_DEPTH, WOKERS_PER_CORE


@click.command()
@click.argument('webpages', nargs=-1)
@click.option('--depth', '-d', default=MAX_DEPTH, help='Number of levels to explore')
@click.option('--output', '-o', default=None, type=click.Path(exists=True), help='The folder in wich to create the CSVs')
@click.option('--workers', '-w', default=WOKERS_PER_CORE, help='Number of workers per processor core to use')
@click.option('--robots', '-R', is_flag=True, help='Whenever to follow robots.txt')
@click.option('--duplicates', '-D', is_flag=True, help='Whenever to allow duplicates (Not Recomended)')
@click.option('--external', '-E', is_flag=True, help='Whenever to allow external sites (Not Recomended)')
@click.option('--regex', default=None, help='Custom regex to filter URLs')
def process_sites(webpages, depth, output, workers, robots, duplicates, external, regex):
    _process_sites(webpages, depth, output, workers, robots, duplicates, external, regex)

def _process_sites(webpages, depth, output, workers, robots, duplicates, external, regex):
    logging.info('''Starting Extraction for {webpages},
    extracting {depth} levels,
    outputing to {output},
    using {workers} workers per core,
    folowing robots.txt: {robots},
    allowing duplicates: {duplicates},
    allowing external sites: {external},
    custom regex: {regex},
    '''.format(**locals()))
    web_crawler = WebCrawler(workers)
    try:
        for website in webpages:
            try:
                site = urlparse(website)
                _database = database.FileDatabase(site, output)
                _filter = filters.BasicFilter(site, _database, robots, duplicates, external, regex)
                web_crawler.process(site, depth, _database, _filter)
            except KeyboardInterrupt:
                logging.warning('Stoping file procesing')
                break
    finally:
        logging.info((
            'Processed {} websites, '
            'for a total of {} nodes '
            'in {} minutes').format(
                web_crawler.total_sites_processed,
                web_crawler.total_worker_count,
                web_crawler.total_time/60))


if __name__ == '__main__':
    process_sites()
