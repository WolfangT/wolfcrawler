"""
database.py

Contains all the diferent methods to store and read information
"""

import logging
from threading import Lock
from csv import writer
from os import path, getcwd


class FileDatabase():
    """A file-based database"""

    def __init__(self, site, folder=None):
        self.site = site
        self.folder = folder if not folder is None else getcwd()
        self.lock = Lock()
        self.manifest = []

    @property
    def filename(self):
        return path.join(self.folder, '{}.cvs'.format(self.site.netloc))

    def add_node(self, node):
        with self.lock:
            self.manifest.append(node['url'])

    def check_node(self, node):
        with self.lock:
            return node['url'] in self.manifest

    def store(self):
        with open(self.filename, 'w') as csvfile:
            csv_writer = writer(csvfile, delimiter=',', quotechar='"')
            for url in self.manifest:
                csv_writer.writerow([url.geturl()])



