"""
filters.py

filters functions
"""

import logging
from urllib.robotparser import RobotFileParser
from urllib.parse import urljoin
import re


class BasicFilter:
    """
    Basic filter that:
    - doesn't allows duplicates
    - doest allows external sites
    - follows robots.txt
    """

    def __init__(self, site, database, robots, duplicates, external, regex):
        self.site = site
        self.database = database
        self.robots = robots
        self.duplicates = duplicates
        self.external = external
        self.regex = regex
        if robots:
            self.rp = RobotFileParser()
            robots_url = urljoin(site.geturl(), '/robots.txt')
            self.rp.set_url(robots_url)
            self.rp.read()
            logging.info('Read contens of {}'.format(robots_url))
        if regex:
            self.pattern = re.compile(regex)

    def __call__(self, node):
        if not self.external:
            if node['url'].netloc != self.site.netloc:
                return False
            if not node['url'].path.startswith(self.site.path):
                return False
        if not self.duplicates:
            if self.database.check_node(node):
                return False
        if self.robots:
            if not self.rp.can_fetch('*', node['url'].geturl()):
                return False
        if self.regex:
            if not self.pattern.match(node['url'].geturl()):
                return False
        return True
