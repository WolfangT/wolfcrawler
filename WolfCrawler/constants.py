"""
constants.py

contains all the constant
"""

# Extractor
RATE_OF_LOGGER = 50 # number of nodes before logging status
WOKERS_PER_CORE = 100 # number of thread per procesor core to be use
MAX_DEPTH = 2 # maximun depth for extration
TIMEOUT = 25

# regex
GENERAL = "(\w+):\/\/([\w\.]+)\/(t[\w\/\?\=\:\-\&\%]+)"
