"""
extractor.py

Module containing the extractor and worker objects
"""

import logging
from time import time, sleep, strftime
from collections import deque
from os import cpu_count
from multiprocessing import Event
from multiprocessing.pool import ThreadPool
from queue import Queue, Empty

from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.parse import urlparse, urljoin, urlunparse
from http.client import BadStatusLine

from .constants import WOKERS_PER_CORE, RATE_OF_LOGGER, TIMEOUT


class Worker:
    """Thread based node traverser"""

    def __init__(self, max_depth, database, _filter, execution_flag, name):
        self.max_depth = max_depth
        self.database = database
        self._filter = _filter
        self.execution_flag = execution_flag
        self.name = name

    def __call__(self, node, node_number):
        self.execution_flag.wait()
        try:
            starting_time = time()
            logging.debug('{} Starting node {}: (level {}) {}'.format(
                self, node_number, node['depth'], node['url'].geturl()))
            childrens = self._process_node(node)
            filtered = list(filter(self._filter, childrens))
            return filtered
        except KeyboardInterrupt:
            logging.warning('{} Catched KeyboardInterrupt'.format(self))
        finally:
            total_time = time() - starting_time
            logging.debug('{} finished on {} sec'.format(self, total_time))

    def _process_node(self, node):
        self.database.add_node(node)
        childrens = []
        if node['depth'] < self.max_depth:
            while True:
                try:
                    with urlopen(node['url'].geturl(), timeout=TIMEOUT) as page:
                        soup = BeautifulSoup(page, 'html.parser')
                        for link in soup.find_all('a'):
                            if not link.get('href'): continue # ignore empty
                            if link.get('href')[0] == '#': continue # ignore fragments
                            url = urlparse(urljoin(node['url'].geturl(), link.get('href')))
                            url = urlparse(urlunparse(url[:-1]+('',)))
                            childrens.append({
                                'url':url,
                                'depth':node['depth']+1,
                                })
                except BadStatusLine:
                    pass
                else:
                    break
        return childrens

    def __str__(self):
        return "P{}".format(self.name)


class WebCrawler:
    """Manages the subprocess necesary to build a tree"""

    def __init__(self, workers_per_core=WOKERS_PER_CORE):
        logging.info('Creating a WebCrawler')
        self.execution_flag = Event()
        self.total_worker_count = 0
        self.total_time = 0
        self.total_sites_processed = 0
        self.max_workers = int(cpu_count() * workers_per_core)
        logging.info(('There are {} cores on sistem, '
                      'using {} workers max'
                      ).format(cpu_count(), self.max_workers))

    def process(self, site, max_depth, database, _filter):
        starting_time = time()
        logging.info('At {} Processing site {}'.format(
            strftime("%a, %d %b %Y %H:%M:%S"), site.geturl()))
        workers = self._prepare_workers(max_depth, database, _filter)
        workers_count = self._process(site, workers, database, starting_time)
        database.store()
        self.total_worker_count += workers_count
        self.total_sites_processed += 1
        total_time = time() - starting_time
        self.total_time += total_time
        logging.info(
            'Finished, processed {} nodes on {} seconds'.format(
                workers_count, total_time))

    def _prepare_workers(self, max_depth, database, _filter):
        workers = deque()
        for i in range(self.max_workers):
            workers.append(Worker(max_depth, database, _filter, self.execution_flag, i))
        return workers

    def _log_status(self, starting_time, nodes_procesed, workers_count):
        t = time() - starting_time
        p = t / nodes_procesed if nodes_procesed > 0 else 0
        logging.info((
            "{} nodes procesed in {} sec "
            "for an aprox. of {} sec per node. "
            "{} nodes remain for processing "
            "right now ").format(
                nodes_procesed, t, p,
                workers_count-nodes_procesed))

    def _process(self, site, workers, database, starting_time):
        nodes = Queue()
        nodes.put({
            'url':site,
            'depth':0,
            })
        results = []
        workers_count = 0
        nodes_procesed = 0
        with ThreadPool(processes=self.max_workers) as pool:
            self.execution_flag.set()
            while True:
                for node, result in results:
                    if result.ready():
                        results.remove((node, result))
                        try:
                            for childs in result.get():
                                nodes.put(childs)
                        except Exception as err:
                            logging.error('"{}" ocurred on node {}. returning for re-procesing'.format(err, node['url'].geturl()))
                            nodes.put(node)
                        else:
                            nodes_procesed += 1
                        if nodes_procesed % RATE_OF_LOGGER == 0:
                            self._log_status(starting_time, nodes_procesed, workers_count)
                while True:
                    try:
                        node = nodes.get_nowait()
                        workers.rotate()
                        workers_count += 1
                        results.append((node, pool.apply_async(workers[0], (node, workers_count))))
                    except Empty:
                        break
                if len(results) == 0 and nodes.empty():
                    break
                sleep(0.1/self.max_workers)
        return workers_count
